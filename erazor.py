import os

count = int(input("amount: "))
delete = input("delete? (y/n): ")
size = int(input("size per file (gb): "))

a = 0
while (a < count):
    name = "dump" + str(a) + ".bin"
    f = open(name, "ab")

    num = 0
    while (num < (0xfffff*size)):
        f.write(os.urandom(1024))
        num = num + 1
        
    f.close()

    if os.path.exists(name):
        if delete == 'y':
            os.remove(name)
        print(f"pass {a + 1}")
        a = a + 1
    else:
        if delete == 'y':
            print(f"failed to delete dump{a}.bin")
        a = a + 1
    
print("done")